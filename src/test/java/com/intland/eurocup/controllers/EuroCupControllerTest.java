package com.intland.eurocup.controllers;

import com.intland.eurocup.model.RedeemRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class EuroCupControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    @Sql(scripts = {"classpath:com/intland/eurocup/repositories/repositories.testCleanup.sql"},
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void shouldReturnWinnerPageForFirstTime() throws Exception {
        this.performRedeemPost(new RedeemRequest(1, "a@b.com", "0123456789", ""))
                .andExpect(content().string(containsString("Congratulations you won!")));
    }

    @Test
    @Sql(scripts = {"classpath:com/intland/eurocup/repositories/repositories.testCleanup.sql"},
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void shouldReturnValidationErrorForInvalidEmail() throws Exception {
        this.performRedeemPost(new RedeemRequest(1, "", "0123456789", ""))
                .andExpect(content().string(containsString("E-mail address is mandatory.")));

        this.performRedeemPost(new RedeemRequest(1, "testtest", "0123456789", ""))
                .andExpect(content().string(containsString("Invalid e-mail address.")));

        this.performRedeemPost(new RedeemRequest(1, "testtest.com", "0123456789", ""))
                .andExpect(content().string(containsString("Invalid e-mail address.")));
    }

    @Test
    @Sql(scripts = {"classpath:com/intland/eurocup/repositories/repositories.testCleanup.sql"},
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void shouldReturnValidationErrorForMissingCoupon() throws Exception {
        this.performRedeemPost(new RedeemRequest(1, "test@test.com", "", ""))
                .andExpect(content().string(containsString("Coupon code is mandatory.")));

        this.performRedeemPost(new RedeemRequest(1, "testtest", null, ""))
                .andExpect(content().string(containsString("Coupon code is mandatory.")));

        this.performRedeemPost(new RedeemRequest(1, "testtest", "123443", ""))
                .andExpect(content().string(containsString("Coupon code must be 10 character long.")));
    }

    @Test
    @Sql(scripts = {"classpath:com/intland/eurocup/repositories/repositories.testCleanup.sql"},
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void shouldReturnValidationErrorForInvalidTerritory() throws Exception {
        this.performRedeemPost(new RedeemRequest(41, "test@test.com", "0123456789", ""))
                .andExpect(content().string(containsString("Invalid territory")));
    }

    private ResultActions performRedeemPost(RedeemRequest redeemRequest) throws Exception {
        return this.mockMvc
                .perform(post("/redeem")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .flashAttr("request", redeemRequest)
                )
                .andDo(print()).andExpect(status().isOk());

    }
}
