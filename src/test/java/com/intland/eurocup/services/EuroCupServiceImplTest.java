package com.intland.eurocup.services;

import com.intland.eurocup.model.QueueItem;
import com.intland.eurocup.model.RedeemRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class EuroCupServiceImplTest {
    @Spy
    private BlockingQueue<QueueItem> requestQueue = new ArrayBlockingQueue<>(1);

    @InjectMocks
    private EuroCupServiceImpl euroCupService;

    @Test
    public void shouldProcessRequest(){
        RedeemRequest redeemRequest = new RedeemRequest(1, "test@test.com", "1234567890", "A str. 12.");
        euroCupService.processRequest(redeemRequest);
        assertEquals(1, requestQueue.size());
    }
}
