package com.intland.eurocup.services;

import com.intland.eurocup.model.LotResult;
import com.intland.eurocup.model.RedeemRequest;
import com.intland.eurocup.model.RedeemResult;
import com.intland.eurocup.model.Territory;
import com.intland.eurocup.repositories.TerritoryRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EuroCupServiceBatchTest {

    Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    EuroCupService euroCupService;
    @MockBean
    private TerritoryRepository territoryRepository;

    @Before
    public void setupTerritories() {
        when(territoryRepository.getById(anyInt())).thenReturn(new Territory(1, "Hungary", 15, 200, 150));
    }

    @Test
    @Sql(scripts = {"classpath:com/intland/eurocup/repositories/repositories.testCleanup.sql"},
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void shouldKeepTheRules() {
        List<RedeemResult> redeemResults = sendRequests(600);

        assertWinnerLoserResults(redeemResults, 40, 560);

    }

    @Test
    @Sql(scripts = {"classpath:com/intland/eurocup/repositories/repositories.testCleanup.sql"},
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void shouldKeepTheRulesParallel() {
        List<RedeemResult> redeemResults = sendRequests(600);

        assertWinnerLoserResults(redeemResults, 40, 560);

    }

    @Test
    @Sql(scripts = {"classpath:com/intland/eurocup/repositories/repositories.testCleanup.sql"},
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void shouldKeepTheRulesWithDailyLimit() {
        when(territoryRepository.getById(anyInt())).thenReturn(new Territory(1, "Hungary", 15, 200, 23));

        List<RedeemResult> redeemResults = sendRequests(600);

        assertWinnerLoserResults(redeemResults, 23, 577);
    }

    @Test
    @Sql(scripts = {"classpath:com/intland/eurocup/repositories/repositories.testCleanup.sql"},
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void shouldKeepTheRulesWithLimit() {
        when(territoryRepository.getById(anyInt())).thenReturn(new Territory(1, "Hungary", 15, 31, 120));

        List<RedeemResult> redeemResults = sendRequests(600);

        assertWinnerLoserResults(redeemResults, 31, 569);
    }


    private void assertWinnerLoserResults(List<RedeemResult> redeemResults, int expectedWinnerCount, int expectedLoserCount) {
        long winnerCount = redeemResults.stream()
                .filter(redeemResult -> redeemResult.getLotResult() == LotResult.Winner)
                .count();
        long loserCount = redeemResults.stream()
                .filter(redeemResult -> redeemResult.getLotResult() == LotResult.Loser)
                .count();

        assertEquals(expectedWinnerCount, winnerCount);
        assertEquals(expectedLoserCount, loserCount);
    }

    private List<RedeemResult> sendRequests(int count) {
        return IntStream.range(0, count).parallel()
                .mapToObj(this::generateRedeemRequest)
                .map(euroCupService::processRequest)
                .map(getFutures())
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    private Function<Future<RedeemResult>, Optional<RedeemResult>> getFutures() {
        return future -> {
            try {
                return Optional.of(future.get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
            return Optional.empty();
        };
    }

    private RedeemRequest generateRedeemRequest(int index) {
        String couponCode = "0000000000".substring(0, 10 - (index + "").length()) + index;
        String email = index + "@amail.com";

        return new RedeemRequest(1, couponCode, email, "A str. 12.");
    }

}
