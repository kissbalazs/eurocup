package com.intland.eurocup.validators;

import com.intland.eurocup.model.Territory;
import com.intland.eurocup.services.EuroCupService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TerritoryIdValidatorTest {
    @Mock
    private
    EuroCupService euroCupService;
    private List<Territory> territories = new ArrayList<>();
    @Mock
    private ConstraintValidatorContext constraintValidatorContext;
    @InjectMocks
    private TerritoryIdValidator territoryIdValidator;

    @Before
    public void setup() {
        territories.add(new Territory(1, "Hungary", 10, 10, 10));
        territories.add(new Territory(2, "Germany", 10, 10, 10));

        when(euroCupService.getAllTerritory()).thenReturn(territories);
    }

    @Test
    public void testIsValidIfFieldIsVaild() {
        assertTrue(territoryIdValidator.isValid(1, constraintValidatorContext));
        assertTrue(territoryIdValidator.isValid(2, constraintValidatorContext));
    }

    @Test
    public void testIsValidIfFieldIsInvaild() {
        assertFalse(territoryIdValidator.isValid(3, constraintValidatorContext));
    }
}
