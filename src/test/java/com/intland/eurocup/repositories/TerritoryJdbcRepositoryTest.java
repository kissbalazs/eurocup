package com.intland.eurocup.repositories;

import com.intland.eurocup.model.Territory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TerritoryJdbcRepositoryTest {

    @Autowired
    private TerritoryJdbcRepository territoryJdbcRepository;

    @Test
    public void testGetAll() {
        List<Territory> territories = territoryJdbcRepository.getAll();
        assertEquals(2, territories.size());

        Territory territory = territories.get(0);

        assertEquals(1, territory.getTerritoryId());
        assertNotNull(territory.getName());
    }

    @Test
    public void testGetById() {
        Territory territory = territoryJdbcRepository.getById(1);

        assertEquals(1, territory.getTerritoryId());
        assertNotNull(territory.getName());
    }
}
