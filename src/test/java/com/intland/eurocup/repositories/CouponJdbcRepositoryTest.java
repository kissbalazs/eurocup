package com.intland.eurocup.repositories;

import com.intland.eurocup.model.Coupon;
import com.intland.eurocup.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CouponJdbcRepositoryTest {

    @Autowired
    private CouponRepository couponRepository;

    private User testUser;

    @Before
    public void setupTestUser() {
        testUser = new User("test@test.com", "A str. 12-", 1);
    }

    @Test
    @Sql(scripts={"classpath:com/intland/eurocup/repositories/repositories.testCleanup.sql"},
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void testInsertionWithCouponCode(){
        String couponCode = "1234567890";
        Coupon coupon = couponRepository.insert(couponCode);
        assertNotNull(coupon);
        assertEquals(couponCode, coupon.getCode());
    }

    @Test
    @Sql(scripts={"classpath:com/intland/eurocup/repositories/repositories.testCleanup.sql"},
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void testIsUsedIfItsUsed(){
        String couponCode = "1234567890";
        boolean isUsed = couponRepository.isUsed(couponCode);
        assertFalse(isUsed);
    }

    @Test
    @Sql(scripts={"classpath:com/intland/eurocup/repositories/repositories.testCleanup.sql"},
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void testIsUsedIfItIsNotUsed(){
        String couponCode = "1234567890";
        couponRepository.insert(couponCode);
        boolean isUsed = couponRepository.isUsed(couponCode);
        assertTrue(isUsed);
    }

    @Test
    @Sql(scripts = {"classpath:com/intland/eurocup/repositories/couponRepository.testInit.sql"},
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(scripts = {"classpath:com/intland/eurocup/repositories/repositories.testCleanup.sql"},
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void testGetCouponCountByTerritoryIfSameTerritory() {

        int couponCountByTerritory = couponRepository.getCouponCountByTerritory(1);

        assertEquals(2, couponCountByTerritory);
    }

    @Test
    @Sql(scripts = {"classpath:com/intland/eurocup/repositories/couponRepository.testInit.sql"},
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(scripts = {"classpath:com/intland/eurocup/repositories/repositories.testCleanup.sql"},
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void testGetCouponCountByTerritoryIfDifferentTerritory() {

        int couponCountByTerritory = couponRepository.getCouponCountByTerritory(2);

        assertEquals(0, couponCountByTerritory);
    }

    @Test
    @Sql(scripts = {"classpath:com/intland/eurocup/repositories/repositories.testCleanup.sql"},
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void testGetCouponCountByTerritoryIfNone() {
        int couponCountByTerritory = couponRepository.getCouponCountByTerritory(testUser.getTerritoryId());

        assertEquals(0, couponCountByTerritory);
    }

    @Test
    @Sql(scripts = {"classpath:com/intland/eurocup/repositories/couponRepository.testInit.sql"},
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(scripts = {"classpath:com/intland/eurocup/repositories/repositories.testCleanup.sql"},
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void testGetWinnerCouponCountByTerritoryIfSameTerritory() {

        int couponCountByTerritory = couponRepository.getWinnerCouponCountByTerritory(1);

        assertEquals(2, couponCountByTerritory);
    }

    @Test
    @Sql(scripts = {"classpath:com/intland/eurocup/repositories/couponRepository.testInit.sql"},
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(scripts = {"classpath:com/intland/eurocup/repositories/repositories.testCleanup.sql"},
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void testGetWinnerCouponCountByTerritoryIfDifferentTerritory() {

        int couponCountByTerritory = couponRepository.getWinnerCouponCountByTerritory(2);

        assertEquals(0, couponCountByTerritory);
    }
}
