package com.intland.eurocup.repositories;

import com.intland.eurocup.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserJdbcRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    @Sql(scripts={"classpath:com/intland/eurocup/repositories/userRepository.testInit.sql"},
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(scripts={"classpath:com/intland/eurocup/repositories/repositories.testCleanup.sql"},
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void testInsertionWithAddress(){
        String email = "test@test.com";
        String address = "János Géza utca 12.";
        int couponId = 1;
        Number userId = userRepository.insert(new User(email, address, 1), couponId);
        User user = userRepository.findById(userId.longValue());
        assertNotNull(user);
        assertEquals(email, user.getEmail());
        assertEquals(address, user.getAddress());
    }

    @Test
    @Sql(scripts={"classpath:com/intland/eurocup/repositories/userRepository.testInit.sql"},
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(scripts={"classpath:com/intland/eurocup/repositories/repositories.testCleanup.sql"},
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void testInsertionWithoutAddress(){
        String email = "test@test.com";
        int couponId = 1;
        Number userId = userRepository.insert(new User(email, null, 1), couponId);
        User user = userRepository.findById(userId.longValue());
        assertNotNull(user);
        assertEquals(email, user.getEmail());
        assertEquals(null, user.getAddress());
    }


    @Test
    @Sql(scripts = {"classpath:com/intland/eurocup/repositories/userRepository.testInit.sql"},
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(scripts = {"classpath:com/intland/eurocup/repositories/repositories.testCleanup.sql"},
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void testIsEmailExtists() {

        assertTrue(userRepository.isEmailExists("testA@test.com"));

        assertFalse(userRepository.isEmailExists("test@test.com"));
    }

    @Test
    @Sql(scripts = {"classpath:com/intland/eurocup/repositories/userRepository.testInit.sql"},
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(scripts = {"classpath:com/intland/eurocup/repositories/repositories.testCleanup.sql"},
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void testGetWinnerCountByTerritory() {

        assertEquals(2, userRepository.getWinnerCountByTerritory(1));

        assertEquals(0, userRepository.getWinnerCountByTerritory(2));
    }

}
