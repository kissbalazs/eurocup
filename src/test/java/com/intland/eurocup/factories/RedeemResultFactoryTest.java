package com.intland.eurocup.factories;

import com.intland.eurocup.model.RedeemResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.FieldError;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class RedeemResultFactoryTest {
    @Test
    public void shouldReturnARequestErrorObject(){
        RedeemResult redeemResultWithError = RedeemResultFactory.createRedeemResultWithError("email", "E-mail error.");
        FieldError fieldError = redeemResultWithError.getValidationErrors().get(0);
        assertEquals("request", fieldError.getObjectName());
        assertEquals("email", fieldError.getField());
        assertEquals("E-mail error.", fieldError.getDefaultMessage());
    }
}
