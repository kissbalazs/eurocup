package com.intland.eurocup.threads;

import com.intland.eurocup.model.*;
import com.intland.eurocup.repositories.CouponRepository;
import com.intland.eurocup.repositories.TerritoryRepository;
import com.intland.eurocup.repositories.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.validation.FieldError;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RequestProcessorThreadTest {

    @Mock
    private UserRepository userRepository;
    @Mock
    private CouponRepository couponRepository;
    @Mock
    private TerritoryRepository territoryRepository;

    @InjectMocks
    private RequestProcessorThread requestProcessorThread;


    private String email = "test@test.com";
    private String couponCode = "1234567890";
    private Coupon coupon = new Coupon(1, couponCode);
    private Territory territory = new Territory(1, "Hungary", 10, 10, 10);
    private RedeemRequest redeemRequest;

    @Before
    public void setup() {
        redeemRequest = new RedeemRequest(1, email, couponCode, "A str. 12.");

        when(territoryRepository.getById(anyInt())).thenReturn(territory);

        when(userRepository.isEmailExists(email)).thenReturn(false);
        when(userRepository.getWinnerCountByTerritory(anyInt())).thenReturn(0);
        when(userRepository.insert(any(), anyInt())).thenReturn(1);

        when(couponRepository.isUsed(couponCode)).thenReturn(false);
        when(couponRepository.getWinnerCouponCountByTerritory(anyInt())).thenReturn(0);
        when(couponRepository.getCouponCountByTerritory(anyInt())).thenReturn(0);
        when(couponRepository.insert(couponCode)).thenReturn(coupon);

    }


    @Test
    public void shouldPersistItem() throws ExecutionException, InterruptedException {
        CompletableFuture<RedeemResult> redeemResultFuture = new CompletableFuture<>();
        QueueItem queueItem = new QueueItem(redeemRequest, redeemResultFuture);
        queueItem.setCoupon(coupon);

        requestProcessorThread.processRequest(queueItem);
        RedeemResult redeemResult = redeemResultFuture.get();


        assertEquals(LotResult.Winner, redeemResult.getLotResult());

        verify(couponRepository, times(1)).insert(any());
        verify(userRepository, times(1)).insert(any(), anyInt());
    }


    @Test
    public void shouldReturnAnErrorIfEmailIsUsed() throws ExecutionException, InterruptedException {
        CompletableFuture<RedeemResult> redeemResultFuture = new CompletableFuture<>();
        Coupon coupon = new Coupon(1, couponCode);
        QueueItem queueItem = new QueueItem(redeemRequest, redeemResultFuture);
        queueItem.setCoupon(coupon);

        when(userRepository.isEmailExists(email)).thenReturn(true);

        requestProcessorThread.processRequest(queueItem);
        RedeemResult redeemResult = redeemResultFuture.get();
        FieldError redeemError = redeemResult.getValidationErrors().get(0);

        assertEquals("request", redeemError.getObjectName());
        assertEquals("email", redeemError.getField());
        assertEquals("E-mail is already in use.", redeemError.getDefaultMessage());

        verify(couponRepository, times(0)).insert(any());
        verify(userRepository, times(0)).insert(any(), anyInt());
    }


    @Test
    public void shouldReturnAnErrorIfcouponCodeIsUsed() throws ExecutionException, InterruptedException {
        RedeemRequest redeemRequest = new RedeemRequest(1, email, couponCode, "A str. 12.");
        CompletableFuture<RedeemResult> redeemResultFuture = new CompletableFuture<>();
        Coupon coupon = new Coupon(1, couponCode);
        QueueItem queueItem = new QueueItem(redeemRequest, redeemResultFuture);
        queueItem.setCoupon(coupon);

        when(couponRepository.isUsed(couponCode)).thenReturn(true);

        requestProcessorThread.processRequest(queueItem);
        RedeemResult redeemResult = redeemResultFuture.get();
        FieldError redeemError = redeemResult.getValidationErrors().get(0);

        assertEquals("request", redeemError.getObjectName());
        assertEquals("couponCode", redeemError.getField());
        assertEquals("The coupon is already in use.", redeemError.getDefaultMessage());

        verify(couponRepository, times(0)).insert(any());
        verify(userRepository, times(0)).insert(any(), anyInt());
    }
}
