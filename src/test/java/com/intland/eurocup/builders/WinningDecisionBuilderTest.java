package com.intland.eurocup.builders;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

@RunWith(SpringRunner.class)
public class WinningDecisionBuilderTest {

    private WinningDecisionBuilder winningDecisionBuilder;

    @Before
    public void setup() {
        winningDecisionBuilder = new WinningDecisionBuilder()
                .withCouponNumber(0)
                .withWinningRate(40)
                .withWinnerCount(0)
                .withMaxWinners(1000)
                .withMaxDailyWinners(250)
                .withWinnerCouponCountToday(0);
    }

    @Test
    public void shouldDecideToBeWinnerForFirst() {
        assertTrue(winningDecisionBuilder
                .makeDecision());
    }

    @Test
    public void shouldDecideToBeWinnerForWinnerRate() {
        assertTrue(winningDecisionBuilder
                .withWinningRate(40)
                .withCouponNumber(40)
                .makeDecision());
        assertTrue(winningDecisionBuilder
                .withWinningRate(40)
                .withCouponNumber(80)
                .makeDecision());
        assertTrue(winningDecisionBuilder
                .withWinningRate(21)
                .withCouponNumber(42)
                .makeDecision());
    }

    @Test
    public void shouldDecideToBeLoserForNotWinnerRate() {
        assertFalse(winningDecisionBuilder
                .withWinningRate(40)
                .withCouponNumber(41)
                .makeDecision());
        assertFalse(winningDecisionBuilder
                .withWinningRate(40)
                .withCouponNumber(58)
                .makeDecision());
        assertFalse(winningDecisionBuilder
                .withWinningRate(21)
                .withCouponNumber(40)
                .makeDecision());
    }

    @Test
    public void shouldDecideToBeLoserOverDailyLimit() {
        assertFalse(winningDecisionBuilder
                .withMaxDailyWinners(30)
                .withWinnerCouponCountToday(31)
                .withCouponNumber(40)
                .makeDecision());
        assertFalse(winningDecisionBuilder
                .withMaxDailyWinners(50)
                .withWinnerCouponCountToday(50)
                .withCouponNumber(80)
                .makeDecision());
        assertFalse(winningDecisionBuilder
                .withMaxDailyWinners(23)
                .withWinnerCouponCountToday(42)
                .withCouponNumber(42)
                .makeDecision());
    }

    @Test
    public void shouldDecideToBeLoserOverWinnerLimit() {
        assertFalse(winningDecisionBuilder
                .withMaxWinners(30)
                .withWinnerCount(31)
                .withCouponNumber(40)
                .makeDecision());
        assertFalse(winningDecisionBuilder
                .withMaxWinners(50)
                .withWinnerCount(50)
                .withCouponNumber(80)
                .makeDecision());
        assertFalse(winningDecisionBuilder
                .withMaxWinners(23)
                .withWinnerCount(42)
                .withCouponNumber(42)
                .makeDecision());
    }
}
