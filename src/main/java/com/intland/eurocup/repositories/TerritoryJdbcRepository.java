package com.intland.eurocup.repositories;

import com.intland.eurocup.model.Territory;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TerritoryJdbcRepository implements TerritoryRepository {
    private final JdbcTemplate jdbcTemplate;

    public TerritoryJdbcRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Territory> getAll() {
        return jdbcTemplate.query("select * from territories", new BeanPropertyRowMapper(Territory.class));
    }

    @Override
    public Territory getById(int territoryId) {
        return jdbcTemplate.queryForObject("select * from territories where territoryId= ?", new Object[]{territoryId}, new BeanPropertyRowMapper<>(Territory.class));
    }
}