package com.intland.eurocup.repositories;

import com.intland.eurocup.model.Coupon;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

import static com.intland.eurocup.utils.DateUtility.getTodayFormatted;

@Repository
public class CouponJdbcRepository implements CouponRepository {
    private final JdbcTemplate jdbcTemplate;

    public CouponJdbcRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Coupon insert(String couponCode) {
        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        jdbcInsert.withTableName("coupons").usingGeneratedKeyColumns(
                "couponId");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("code", couponCode);
        parameters.put("create_datetime", getTodayFormatted());

        Number couponId = jdbcInsert.executeAndReturnKey(new MapSqlParameterSource(
                parameters));
        return new Coupon(couponId.intValue(), couponCode);
    }

    @Override
    public boolean isUsed(String couponCode) {
        Integer couponCount = jdbcTemplate.queryForObject("select count(*) from coupons where code=?", Integer.class, couponCode);
        return couponCount > 0;
    }

    @Override
    public int getCouponCountByTerritory(int territoryId) {
        return jdbcTemplate.queryForObject(
                "select count(*) from users u inner join coupons c on u.couponId = c.couponId where u.territoryId=?",
                Integer.class,
                territoryId
        );
    }

    @Override
    public int getWinnerCouponCountByTerritory(int territoryId) {
        return jdbcTemplate.queryForObject(
                "select count(*) from users u inner join coupons c on u.couponId = c.couponId where u.territoryId=? and u.isWinner=true",
                Integer.class,
                territoryId
        );
    }
}