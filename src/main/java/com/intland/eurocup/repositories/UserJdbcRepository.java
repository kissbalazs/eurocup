package com.intland.eurocup.repositories;

import com.intland.eurocup.model.User;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class UserJdbcRepository implements UserRepository {
    private final JdbcTemplate jdbcTemplate;

    public UserJdbcRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Number insert(User user, int couponId) {
        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        jdbcInsert.withTableName("users").usingGeneratedKeyColumns(
                "userId");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("email", user.getEmail());
        parameters.put("address", user.getAddress());
        parameters.put("couponId", couponId);
        parameters.put("territoryId", user.getTerritoryId());
        parameters.put("isWinner", user.isWinner());

        return jdbcInsert.executeAndReturnKey(new MapSqlParameterSource(
                parameters));
    }

    public User findById(long id) {
        return jdbcTemplate.queryForObject("select * from users where userId=?", new Object[]{id},
                new BeanPropertyRowMapper<>(User.class));
    }

    @Override
    public boolean isEmailExists(String email) {
        Integer emailCount = jdbcTemplate.queryForObject(
                "select count(*) from users where email=?",
                Integer.class,
                email
        );
        return emailCount > 0;
    }

    @Override
    public int getWinnerCountByTerritory(int territoryId) {
        return jdbcTemplate.queryForObject(
                "select count(*) from users where territoryId=? and isWinner=true",
                Integer.class,
                territoryId
        );
    }
}
