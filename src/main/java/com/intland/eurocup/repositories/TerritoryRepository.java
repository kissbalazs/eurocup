package com.intland.eurocup.repositories;

import com.intland.eurocup.model.Territory;

import java.util.List;

public interface TerritoryRepository {
    List<Territory> getAll();
    Territory getById(int territoryId);
}
