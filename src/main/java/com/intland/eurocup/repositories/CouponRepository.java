package com.intland.eurocup.repositories;

import com.intland.eurocup.model.Coupon;

public interface CouponRepository {
    Coupon insert(String couponCode);

    boolean isUsed(String couponCode);

    int getCouponCountByTerritory(int territoryId);

    int getWinnerCouponCountByTerritory(int territoryId);
}
