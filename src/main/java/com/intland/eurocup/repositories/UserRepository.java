package com.intland.eurocup.repositories;

import com.intland.eurocup.model.User;

public interface UserRepository {
    Number insert(User user, int couponId);

    boolean isEmailExists(String email);

    User findById(long id);

    int getWinnerCountByTerritory(int territoryId);
}
