package com.intland.eurocup.contollers;

import com.intland.eurocup.model.LotResult;
import com.intland.eurocup.model.RedeemRequest;
import com.intland.eurocup.model.RedeemResult;
import com.intland.eurocup.services.EuroCupService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Controller
public class EuroCupController {

    private EuroCupService euroCupService;

    public EuroCupController(EuroCupService euroCupService) {
        this.euroCupService = euroCupService;
    }

    @PostMapping("/redeem")
    public String redeemCoupon(@Validated @ModelAttribute("request") RedeemRequest request, BindingResult bindingResult, Model model) {
        model.addAttribute("request", request);
        model.addAttribute("territories", euroCupService.getAllTerritory());
        if (bindingResult.hasErrors()) {
            return "index";
        }
        try {
            Future<RedeemResult> redeemResultFuture = euroCupService.processRequest(request);
            RedeemResult redeemResult = redeemResultFuture.get();
            if(redeemResult.hasValidationErrors()) {
                redeemResult.getValidationErrors().forEach(bindingResult::addError);
                return "index";
            }
            else if (redeemResult.getLotResult() == LotResult.Winner) {
                return "result.winner";
            }
            else if (redeemResult.getLotResult() == LotResult.Loser) {
                return "result.loser";
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return "error";
    }

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("request", new RedeemRequest(2));
        model.addAttribute("territories", euroCupService.getAllTerritory());
        return "index";
    }
}
