package com.intland.eurocup.builders;

public class WinningDecisionBuilder {
    private int couponNumber;
    private int winnerCouponCountToday;
    private int maxDailyWinners;
    private int winningRate;
    private int winnerCount;
    private int maxWinners;

    public WinningDecisionBuilder withCouponNumber(int couponNumber) {
        this.couponNumber = couponNumber;
        return this;
    }

    public WinningDecisionBuilder withWinnerCouponCountToday(int winnerCouponCountToday) {
        this.winnerCouponCountToday = winnerCouponCountToday;
        return this;
    }

    public WinningDecisionBuilder withMaxDailyWinners(int maxDailyWinners) {
        this.maxDailyWinners = maxDailyWinners;
        return this;
    }

    public WinningDecisionBuilder withWinningRate(int winningRate) {
        this.winningRate = winningRate;
        return this;
    }

    public WinningDecisionBuilder withWinnerCount(int winnerCount) {
        this.winnerCount = winnerCount;
        return this;
    }

    public WinningDecisionBuilder withMaxWinners(int maxWinners) {
        this.maxWinners = maxWinners;
        return this;
    }
    public boolean makeDecision(){
        return winnerCouponCountToday < maxDailyWinners && couponNumber % winningRate == 0 && winnerCount < maxWinners;
    }
}
