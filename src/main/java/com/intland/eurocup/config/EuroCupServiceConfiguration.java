package com.intland.eurocup.config;

import com.intland.eurocup.model.QueueItem;
import com.intland.eurocup.repositories.CouponRepository;
import com.intland.eurocup.repositories.TerritoryRepository;
import com.intland.eurocup.repositories.UserRepository;
import com.intland.eurocup.services.EuroCupService;
import com.intland.eurocup.services.EuroCupServiceImpl;
import com.intland.eurocup.services.RequestProcessorService;
import com.intland.eurocup.services.RequestProcessorServiceImpl;
import com.intland.eurocup.threads.RequestProcessorThread;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

@Configuration
public class EuroCupServiceConfiguration {

    @Bean
    public BlockingQueue<QueueItem> requestQueue(){
        return new ArrayBlockingQueue(100);
    }

    @Bean
    public EuroCupService euroCupService(BlockingQueue<QueueItem> requestQueue, TerritoryRepository territoryRepository){
        return new EuroCupServiceImpl(requestQueue, territoryRepository);
    }

    @Bean
    public RequestProcessorService requestProcessorService(RequestProcessorThread requestProcessorThread) {
        return new RequestProcessorServiceImpl(requestProcessorThread);
    }

    @Bean
    public RequestProcessorThread requestPersistorThread(
            BlockingQueue<QueueItem> requestQueue,
            UserRepository userRepository,
            CouponRepository couponRepository,
            TerritoryRepository territoryRepository) {
        return new RequestProcessorThread(requestQueue, userRepository, couponRepository, territoryRepository);
    }
}
