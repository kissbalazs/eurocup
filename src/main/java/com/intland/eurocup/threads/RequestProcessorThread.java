package com.intland.eurocup.threads;

import com.intland.eurocup.builders.WinningDecisionBuilder;
import com.intland.eurocup.factories.RedeemResultFactory;
import com.intland.eurocup.model.*;
import com.intland.eurocup.repositories.CouponRepository;
import com.intland.eurocup.repositories.TerritoryRepository;
import com.intland.eurocup.repositories.UserRepository;
import com.intland.eurocup.services.RequestProcessorServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;

public class RequestProcessorThread extends Thread {

    private final BlockingQueue<QueueItem> requestQueue;
    private final UserRepository userRepository;
    private final CouponRepository couponRepository;
    private final TerritoryRepository territoryRepository;

    Logger logger = LoggerFactory.getLogger(RequestProcessorServiceImpl.class);

    public RequestProcessorThread(BlockingQueue<QueueItem> requestQueue,
                                  UserRepository userRepository,
                                  CouponRepository couponRepository,
                                  TerritoryRepository territoryRepository) {
        this.requestQueue = requestQueue;
        this.userRepository = userRepository;
        this.couponRepository = couponRepository;
        this.territoryRepository = territoryRepository;
    }

    @Override
    public void run() {
        while (true) {
            try {
                this.processRequest(requestQueue.take());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    void processRequest(QueueItem queueItem) {
        CompletableFuture<RedeemResult> redeemResultFuture = queueItem.getRedeemResultFuture();
        RedeemRequest redeemRequest = queueItem.getRedeemRequest();

        String couponCode = redeemRequest.getCouponCode();
        String email = redeemRequest.getEmail();
        String address = redeemRequest.getAddress();
        int territoryId = redeemRequest.getTerritoryId();


        if (couponRepository.isUsed(couponCode)) {
            redeemResultFuture.complete(RedeemResultFactory.createRedeemResultWithError("couponCode", "The coupon is already in use."));
            return;
        }

        if (userRepository.isEmailExists(email)) {
            redeemResultFuture.complete(RedeemResultFactory.createRedeemResultWithError("email", "E-mail is already in use."));
            return;
        }


        User user = new User(email, address, territoryId);

        if(isWinnerCoupon(user.getTerritoryId())) {
            user.setWinner(true);
            redeemResultFuture.complete(new RedeemResult(LotResult.Winner));
        }
        else {
            user.setWinner(false);
            redeemResultFuture.complete(new RedeemResult(LotResult.Loser));
        }


        Coupon coupon = couponRepository.insert(couponCode);
        userRepository.insert(user, coupon.getCouponId());
    }

    boolean isWinnerCoupon(int territoryId) {
        Territory territory = territoryRepository.getById(territoryId);

        return new WinningDecisionBuilder()
                .withWinnerCouponCountToday(couponRepository.getWinnerCouponCountByTerritory(territoryId))
                .withWinnerCount(userRepository.getWinnerCountByTerritory(territoryId))
                .withMaxDailyWinners(territory.getMaxDailyWinners())
                .withMaxWinners(territory.getMaxWinners())
                .withWinningRate(territory.getWinningRate())
                .withCouponNumber(couponRepository.getCouponCountByTerritory(territoryId))
                .makeDecision();
    }

}