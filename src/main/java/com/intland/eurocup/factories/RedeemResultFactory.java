package com.intland.eurocup.factories;

import com.intland.eurocup.model.RedeemResult;
import org.springframework.validation.FieldError;

public class RedeemResultFactory {
    public static RedeemResult createRedeemResultWithError( String fieldName, String message) {
        return new RedeemResult(new FieldError("request", fieldName, message));
    }
}
