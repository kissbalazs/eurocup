package com.intland.eurocup.validators;

import org.springframework.context.annotation.Configuration;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Configuration
@Documented
@Constraint(validatedBy = TerritoryIdValidator.class)
@Target( { ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface TerritoryConstraint {
    String message() default "Invalid territory";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
