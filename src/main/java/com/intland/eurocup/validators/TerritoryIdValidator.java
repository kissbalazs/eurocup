package com.intland.eurocup.validators;

import com.intland.eurocup.services.EuroCupService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class TerritoryIdValidator implements
        ConstraintValidator<TerritoryConstraint, Integer> {
 
    @Override
    public void initialize(TerritoryConstraint contactNumber) {
    }

    @Autowired
    EuroCupService euroCupService;
 
    @Override
    public boolean isValid(Integer territoryField, ConstraintValidatorContext cxt) {
        return euroCupService.getAllTerritory().stream().anyMatch(territory -> territory.getTerritoryId() == territoryField);
    }
 
}