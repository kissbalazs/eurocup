package com.intland.eurocup.model;

import com.intland.eurocup.validators.TerritoryConstraint;

import javax.validation.constraints.*;

public class RedeemRequest {
    @NotNull(message = "Territory is mandatory.")
    @TerritoryConstraint
    private int territoryId;

    @NotNull(message = "E-mail address is mandatory.")
    @NotEmpty(message = "E-mail address is mandatory.")
    @Email(message = "Invalid e-mail address.")
    private String email;

    @NotNull(message = "Coupon code is mandatory.")
    @NotEmpty(message = "Coupon code is mandatory.")
    @Size(min = 10, max = 10, message = "Coupon code must be 10 character long.")
    private String couponCode;

    private String address;

    @NotNull(message = "You must agree with the Terms and Conditions.")
    @AssertTrue(message = "You must agree with the Terms and Conditions.")
    private boolean agreedTerms;

    public RedeemRequest() {
    }

    public RedeemRequest(@NotNull(message = "Territory is mandatory.") @NotEmpty(message = "Territory is mandatory.") int territoryId) {
        this.territoryId = territoryId;
    }


    public RedeemRequest(
            @NotNull(message = "Territory is mandatory.") int territoryId,
            @NotNull(message = "E-mail address is mandatory.") @NotEmpty(message = "E-mail address is mandatory.") @Email(message = "Invalid e-mail address.") String email,
            @NotNull(message = "Coupon code is mandatory.") @NotEmpty(message = "Coupon code is mandatory.") @Size(min = 10, max = 10, message = "Coupon code must be 10 character long.") String couponCode,
            String address,
            @NotNull(message = "You must agree with the Terms and Conditions.") @AssertTrue(message = "You must agree with the Terms and Conditions.") boolean agreedTerms) {
        this.territoryId = territoryId;
        this.email = email;
        this.couponCode = couponCode;
        this.address = address;
        this.agreedTerms = agreedTerms;
    }

    public int getTerritoryId() {
        return territoryId;
    }

    public void setTerritoryId(int territoryId) {
        this.territoryId = territoryId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isAgreedTerms() {
        return agreedTerms;
    }

    public void setAgreedTerms(boolean agreedTerms) {
        this.agreedTerms = agreedTerms;
    }
}
