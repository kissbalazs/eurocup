package com.intland.eurocup.model;

import java.util.concurrent.CompletableFuture;

public class QueueItem {
    private RedeemRequest redeemRequest;
    private CompletableFuture<RedeemResult> redeemResultFuture;
    private Coupon coupon;

    public QueueItem(RedeemRequest redeemRequest, CompletableFuture<RedeemResult> redeemResultFuture) {
        this.redeemRequest = redeemRequest;
        this.redeemResultFuture = redeemResultFuture;
    }

    public RedeemRequest getRedeemRequest() {
        return redeemRequest;
    }

    public void setRedeemRequest(RedeemRequest redeemRequest) {
        this.redeemRequest = redeemRequest;
    }

    public CompletableFuture<RedeemResult> getRedeemResultFuture() {
        return redeemResultFuture;
    }

    public void setRedeemResultFuture(CompletableFuture<RedeemResult> redeemResultConsumer) {
        this.redeemResultFuture = redeemResultConsumer;
    }

    public Coupon getCoupon() {
        return coupon;
    }

    public void setCoupon(Coupon coupon) {
        this.coupon = coupon;
    }
}
