package com.intland.eurocup.model;

import java.util.Objects;

public class Coupon {
    private int id;
    private String code;

    public Coupon() {
    }

    public Coupon(int id, String code) {
        this.id = id;
        this.code = code;
    }

    public Coupon(String code) {
        this.code = code;
    }

    public int getCouponId() {
        return id;
    }

    public void setCouponId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coupon coupon = (Coupon) o;
        return id == coupon.id &&
                Objects.equals(code, coupon.code);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, code);
    }
}
