package com.intland.eurocup.model;

public enum LotResult {
    Winner,
    Loser
}
