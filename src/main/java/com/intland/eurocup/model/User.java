package com.intland.eurocup.model;

public class User {
    private Long id;
    private String email;
    private String address;
    private int territoryId;
    private boolean isWinner;

    public User() {
    }

    public User(String email, String address, int territoryId) {
        this.email = email;
        this.address = address;
        this.territoryId = territoryId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getTerritoryId() {
        return territoryId;
    }

    public void setTerritoryId(int territoryId) {
        this.territoryId = territoryId;
    }

    public boolean isWinner() {
        return isWinner;
    }

    public void setWinner(boolean winner) {
        isWinner = winner;
    }
}
