package com.intland.eurocup.model;

import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.List;

public class RedeemResult {
    private List<FieldError> validationErrors = new ArrayList<>();
    private LotResult lotResult;

    public RedeemResult(LotResult lotResult) {
        this.lotResult = lotResult;
    }

    public RedeemResult(FieldError validationError) {
        this.validationErrors.add(validationError);
    }

    public List<FieldError> getValidationErrors() {
        return validationErrors;
    }

    public void setValidationErrors(List<FieldError> validationErrors) {
        this.validationErrors = validationErrors;
    }

    public void addValidationError(FieldError validationError) {
        this.validationErrors.add(validationError);
    }

    public boolean hasValidationErrors(){
        return !validationErrors.isEmpty();
    }

    public LotResult getLotResult() {
        return lotResult;
    }

    public void setLotResult(LotResult lotResult) {
        this.lotResult = lotResult;
    }
}
