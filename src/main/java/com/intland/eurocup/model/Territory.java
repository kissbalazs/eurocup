package com.intland.eurocup.model;

public class Territory {
    private int territoryId;
    private String name;
    private int winningRate;
    private int maxWinners;
    private int maxDailyWinners;

    public Territory() {
    }

    public Territory(int territoryId, String name, int winningRate, int maxWinners, int maxDailyWinners) {
        this.territoryId = territoryId;
        this.name = name;
        this.winningRate = winningRate;
        this.maxWinners = maxWinners;
        this.maxDailyWinners = maxDailyWinners;
    }

    public int getTerritoryId() {
        return territoryId;
    }

    public void setTerritoryId(int territoryId) {
        this.territoryId = territoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWinningRate() {
        return winningRate;
    }

    public void setWinningRate(int winningRate) {
        this.winningRate = winningRate;
    }

    public int getMaxWinners() {
        return maxWinners;
    }

    public void setMaxWinners(int maxWinners) {
        this.maxWinners = maxWinners;
    }

    public int getMaxDailyWinners() {
        return maxDailyWinners;
    }

    public void setMaxDailyWinners(int maxDailyWinners) {
        this.maxDailyWinners = maxDailyWinners;
    }
}
