package com.intland.eurocup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EuroCupApplication {

	public static void main(String[] args) {
		SpringApplication.run(EuroCupApplication.class, args);
	}
}
