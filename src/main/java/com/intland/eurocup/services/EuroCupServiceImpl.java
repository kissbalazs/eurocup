package com.intland.eurocup.services;

import com.intland.eurocup.factories.RedeemResultFactory;
import com.intland.eurocup.model.QueueItem;
import com.intland.eurocup.model.RedeemRequest;
import com.intland.eurocup.model.RedeemResult;
import com.intland.eurocup.model.Territory;
import com.intland.eurocup.repositories.TerritoryRepository;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

public class EuroCupServiceImpl implements EuroCupService {
    private BlockingQueue<QueueItem> requestQueue;
    private TerritoryRepository territoryRepository;

    public EuroCupServiceImpl(BlockingQueue<QueueItem> requestQueue, TerritoryRepository territoryRepository) {
        this.requestQueue = requestQueue;
        this.territoryRepository = territoryRepository;
    }

    @Override
    public Future<RedeemResult> processRequest(RedeemRequest redeemRequest) {
        CompletableFuture<RedeemResult> redeemResultFuture = new CompletableFuture<>();
        try {
            requestQueue.put(new QueueItem(redeemRequest, redeemResultFuture));
            return redeemResultFuture;
        } catch (InterruptedException e) {
            e.printStackTrace();
            redeemResultFuture.complete(RedeemResultFactory.createRedeemResultWithError("couponCode", "Internal error occured."));
            return redeemResultFuture;
        }
    }

    @Override
    public List<Territory> getAllTerritory() {
        return territoryRepository.getAll();
    }
}
