package com.intland.eurocup.services;

import com.intland.eurocup.model.RedeemRequest;
import com.intland.eurocup.model.RedeemResult;
import com.intland.eurocup.model.Territory;

import java.util.List;
import java.util.concurrent.Future;

public interface EuroCupService {
    Future<RedeemResult> processRequest(RedeemRequest redeemRequest);
    List<Territory> getAllTerritory();
}
