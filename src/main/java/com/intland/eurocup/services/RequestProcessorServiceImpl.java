package com.intland.eurocup.services;

import com.intland.eurocup.threads.RequestProcessorThread;

public class RequestProcessorServiceImpl implements RequestProcessorService {

    public RequestProcessorServiceImpl(RequestProcessorThread requestProcessorThread) {
        requestProcessorThread.start();
    }
}
