create table if not exists coupons
(
  couponId integer primary key auto_increment,
  create_datetime datetime,
  code varchar(255) not null
);

create table if not exists territories
(
  territoryId integer primary key auto_increment,
  name varchar(255) not null,
  winningRate integer not null,
  maxWinners integer not null,
  maxDailyWinners integer not null
);

create table if not exists users
(
  userId integer primary key auto_increment,
  email varchar(255) not null,
  address varchar(255),
  couponId integer not null,
  territoryId integer not null,
  isWinner boolean not null,
  FOREIGN KEY (couponId) REFERENCES coupons(couponId),
  FOREIGN KEY (territoryId) REFERENCES territories(territoryId)
);